require 'yaml'

class Services
  def initialize
    @settings ||= YAML.load_file('config/application.yml')
  end

  def scentbird_prod_url
    @settings['scentbird_prod_url']
  end
end
