# Scentbird Automation Tests

### Setup

1. Install Ruby v.2.x using [guide](https://www.ruby-lang.org/en/documentation/installation/),
2. Install [Bundler](https://bundler.io/v1.12/):
`gem install bundler`
3. Clone the [Scentbird repository](https://bitbucket.org/iskhakovan/scentbird.git )
4. Switch to repository directory `cd scentbird`
5. Run `bundle install`

### To run tests
- To run a single test with all examples `cucumber features/feature/gift_page.feature`
- To run a single Cucumber example `cucumber features/feature/gift_page.feature:##`, where `##` is the line number of the example in feature file

Tests run in headless Chrome by default. To run tests in another browser:

- Firefox: `DRIVER=firefox cucumber features/feature/gift_page.feature`
- Chrome: `DRIVER=chrome cucumber features/feature/gift_page.feature`

### Expected test results
[![Expected results](https://www.dropbox.com/s/kzekp7ecfhh7bxz/results.PNG?raw=1 "Expected results")](https://www.dropbox.com/s/kzekp7ecfhh7bxz/results.PNG?raw=1 "Expected results")
