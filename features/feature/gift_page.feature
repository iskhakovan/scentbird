Feature: User on a gift page

    As a user
    In order to buy a gift
    I want to choose gift, fill in a form and proceed to checkout

    Scenario Outline: User can fill in a gift form and proceed to checkout
        Given user on gift page
        When user click on 'Buy the gift for <gender>'
        Then user chooses the gift size <giftSize>
        When user fills in a form
        Then he can choose to send email <notifyDateOption> to notify the recipient of this gift
        And user proceeds to payment page and sees cost <giftCost> in checkout

        Examples:
            | gender | giftSize | giftCost  | notifyDateOption  |
            | her    | 0        | 44        | now               |
            | him    | 2        | 164       | later             |
