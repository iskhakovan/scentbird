require_relative '../../config/services'
require 'active_support'
require 'active_support/core_ext/integer'
require 'active_support/core_ext/numeric'
require 'active_support/core_ext/string'
require 'capybara'
require 'capybara/cucumber'
require 'capybara-screenshot/cucumber'
require 'selenium-webdriver'
require 'pry'
require 'site_prism'
require 'faker'
require 'webdrivers'

Dir[Dir.pwd + '/features/support/**/*.rb'].each do |file|
  require file
end


Capybara.register_driver :driver do |app|
  case ENV['DRIVER']
    when 'chrome'
      Capybara::Selenium::Driver.new(app, browser: :chrome)
    when 'firefox'
      Capybara::Selenium::Driver.new(app, browser: :firefox)
    else
      client = Selenium::WebDriver::Remote::Http::Default.new
      client.read_timeout = 90
      capabilities = Selenium::WebDriver::Remote::Capabilities.chrome(
        chromeOptions: { args: %w(headless disable-gpu) }
      )
      Capybara::Selenium::Driver.new(app, browser: :chrome, desired_capabilities: capabilities, http_client: client)
    end
end

Capybara::Screenshot.register_driver(:driver) do |driver, path|
  driver.browser.save_screenshot(path)
end

Capybara.save_path = "#{Dir.pwd + '/tmp/capybara'}"
Capybara::Screenshot.register_filename_prefix_formatter(:cucumber) do |example|
  "#{example.name}"
end

Capybara.default_max_wait_time =  10
Capybara.default_driver = :driver
Capybara.app_host = Services.new.scentbird_prod_url
