module Pages
    module User
      class GiftPage < SitePrism::Page
        set_url '/gift'

        element :scent_slider, '.slick-slider'
        element :buy_gift_for_her, '#buyForHer', visible:false
        element :buy_gift_for_him, '#buyForHim', visible:false
        element :order_button_small_gift, '#giftSize0Button'
        element :order_button_medium_gift, '#giftSize1Button'
        element :order_button_big_gift, '#giftSize2Button'
        element :submit_modal, '#giftPersonNextButton'
        element :send_email_now, '#giftRightNowButton'
        element :choose_date, '#giftDateButton'
        element :submit_chosen_date, '#giftNextButton'


        def fill_in_gift_modal(recipient_name, recipient_email)
          fill_in 'giftModalRecipientName', with: recipient_name
          fill_in 'giftModalRecipientEmail', with: recipient_email
          fill_in 'giftModalPersonalMessage', with: 'Personal message from user <3 '
          submit_modal.click
        end

        def choose_date_in_calendar_modal
          choose_date.click
          find(:xpath, "//*[@data-testid='pickDateModal']/.//div[text()='#{ 2.days.from_now.strftime('%-d') }']").click
          submit_chosen_date.click
        end

      end
    end
  end
