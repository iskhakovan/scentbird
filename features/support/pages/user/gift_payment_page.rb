module Pages
    module User
      class GiftPaymentPage < SitePrism::Page
        set_url '/gift/payment'

        load_validation { [displayed?, "Expected #{current_url} to match #{url_matcher} but it did not."] }
      end
    end
  end
