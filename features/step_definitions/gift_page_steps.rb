Given(/^user on gift page$/) do
  @gift_page = Pages::User::GiftPage.new
  @gift_page.load
  expect(@gift_page).to have_content('GIVE THE GIFT OF THE SCENT')
  expect(@gift_page).to have_content('The Perfect Gift')
  expect(@gift_page).to have_scent_slider
  expect(@gift_page).to have_content('All the designer fragrances you can imagine in one place')
end

When(/^user click on 'Buy the gift for (.*)'$/) do |gender|
  case gender
  when 'her'
    @gift_page.buy_gift_for_her.click
  when 'him'
    @gift_page.buy_gift_for_him.click
  else
    raise 'Error: no such option. Please add new case to the test step definition'
  end

  expect(@gift_page).to have_content('CHOOSE THE SIZE OF YOUR GIFT')
  expect(@gift_page).to have_content("3-MONTH GIFT\n$44\nFree shipping\nORDER NOW")
  expect(@gift_page).to have_content("6-MONTH GIFT\n$84\nFree shipping\nSave $6\nORDER NOW")
  expect(@gift_page).to have_content("BESTSELLER\n12-MONTH GIFT\n$164\nFree shipping\nSave $15\nORDER NOW")
end

Then(/^user chooses the gift size (.*)$/) do |gift_size|
  case gift_size
  when '0'
    @gift_page.order_button_small_gift.click
  when '1'
    @gift_page.order_button_medium_gift.click
  when '2'
    @gift_page.order_button_big_gift.click
  else
    raise 'Error: no such option. Please add new case to the test step definition'
  end
end

When(/^user fills in a form$/) do
  recipient_name = "#{Faker::Name.first_name} #{Faker::Name.last_name}"
  recipient_email = Faker::Internet.safe_email
  @gift_page.fill_in_gift_modal(recipient_name, recipient_email)
  expect(@gift_page).to have_content('WHEN DO YOU WANT TO NOTIFY THE RECIPIENT OF YOUR GIFT?')
end

Then(/^he can choose to send email (.*) to notify the recipient of this gift$/) do |notify_date_option|
  case notify_date_option
  when 'now'
    @gift_page.send_email_now.click
  when 'later'
    @gift_page.choose_date_in_calendar_modal
  else
    raise 'Error: no such option. Please add new case to the test step definition'
  end
end

And(/^user proceeds to payment page and sees cost (.*) in checkout$/) do |gift_cost|
  @gift_payment_page = Pages::User::GiftPaymentPage.new
  expect(@gift_payment_page).to be_displayed
  expect(@gift_payment_page).to have_content('SECURE CHECKOUT')
  expect(@gift_payment_page).to have_content("Gift subscription\n$#{ gift_cost }")
end
